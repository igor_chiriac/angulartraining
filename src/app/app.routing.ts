import { Routes } from '@angular/router';
import { AircraftsComponent } from './aircrafts/container/aircrafts/aircrafts.component';
export const routes: Routes = [
  {
    path: '',
    redirectTo: '/aircrafts',
    pathMatch : 'full'
  },
  {
     path: 'aircrafts',
     component : AircraftsComponent
  }
];