import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AircraftsComponent } from './container/aircrafts/aircrafts.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AircraftsComponent]
})
export class AircraftsModule { }
