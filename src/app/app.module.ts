import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes } from './app.routing';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MdToolbarModule } from '@angular/material';
import { AircraftsComponent } from './aircrafts/container/aircrafts/aircrafts.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AircraftsComponent
  ],
  imports: [
    BrowserModule,
    MdToolbarModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
